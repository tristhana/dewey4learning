package no.holocon.dewey4Learning.rest;

/**
 * Created by rolfguescini on 01.11.2016.
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath*:dewey4Learning-spring.xml")
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
